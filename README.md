Dies ist das Repo der Projektgruppe in der Veranstaltung "Simulation geo- und solarthermischer Systeme".

Die Aufgaben sind in dem Jupyter Notebook zu finden, benötigte Zeitreihen in dem Unterordner "/data".

Sie können bei Bedarf auch eigene Daten und Bilder in dieses Repository laden. 

Bei Fragen wenden Sie sich gerne an: arjuna.nebel@th-koeln.de
